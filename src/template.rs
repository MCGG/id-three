use std::{fmt, str::FromStr};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct UnknownTemplateParameter;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum TemplateParameter {
    Title,
    TrackNumber,
    Artist,
    Album,
}

pub struct TemplateArgs<'a> {
    template: &'a str,
}

impl FromStr for TemplateParameter {
    type Err = UnknownTemplateParameter;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use self::TemplateParameter::*;

        debug!("s = {}", s);
        match s {
            "%t" => Ok(Title),
            "%n" => Ok(TrackNumber),
            "%a" => Ok(Artist),
            "%A" => Ok(Album),
            _ => Err(UnknownTemplateParameter),
        }
    }
}

impl fmt::Display for TemplateParameter {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

impl TemplateParameter {
    pub fn as_str(&self) -> &'static str {
        use self::TemplateParameter::*;

        trace!("self = {:?}", self);
        match self {
            Title => "%t",
            TrackNumber => "%n",
            Artist => "%a",
            Album => "%A",
        }
    }
}

impl<'a> From<&'a str> for TemplateArgs<'a> {
    fn from(s: &'a str) -> Self {
        trace!("s = {}", s);
        TemplateArgs { template: s }
    }
}

impl<'a> Iterator for TemplateArgs<'a> {
    type Item = TemplateParameter;

    fn next(&mut self) -> Option<Self::Item> {
        debug!("template = {}", self.template);
        while let Some(next) = self.template.find("%") {
            trace!("next = {}", next);
            let item = self.template.get(next..=next + 1);
            debug!("item = {:?}", item);
            self.template = &self.template[next + 1..];

            match item
                .ok_or(UnknownTemplateParameter)
                .and_then(TemplateParameter::from_str)
            {
                Ok(tp) => return Some(tp),
                _ => continue,
            }
        }
        None
    }
}

#[cfg(test)]
mod tests {
    use super::{TemplateArgs, TemplateParameter, UnknownTemplateParameter};
    use std::str::FromStr;

    #[test]
    fn template_args() {
        use self::TemplateParameter::*;
        let mut t_args = TemplateArgs::from("%n - %a sdasd %");
        assert_eq!(t_args.next(), Some(TrackNumber));
        assert_eq!(t_args.next(), Some(Artist));
        assert_eq!(t_args.next(), None);
    }

    #[test]
    fn template_parameter_from_str() {
        use self::TemplateParameter::{self, *};

        assert_eq!(TemplateParameter::from_str("%t"), Ok(Title));
        assert_eq!(TemplateParameter::from_str("%n"), Ok(TrackNumber));
        assert_eq!(TemplateParameter::from_str("%a"), Ok(Artist));
        assert_eq!(TemplateParameter::from_str("%A"), Ok(Album));
        assert_eq!(
            TemplateParameter::from_str("%asdasd"),
            Err(UnknownTemplateParameter)
        );
    }

    #[test]
    fn template_parameter_as_str() {
        use self::TemplateParameter::*;

        assert_eq!("%t", Title.as_str());
        assert_eq!("%n", TrackNumber.as_str());
        assert_eq!("%a", Artist.as_str());
        assert_eq!("%A", Album.as_str());
    }
}
