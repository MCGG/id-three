//Crates
extern crate id3;

extern crate glob;
#[macro_use]
extern crate log;
extern crate chrono;
extern crate fern;

#[macro_use]
extern crate structopt;

//Modules
mod args;
mod tag;
mod template;

//Imports
use std::error::Error;

use args::Args;
use structopt::StructOpt;
use tag::Id3;

//Functions

fn init_logger(args: &Args) -> Result<(), fern::InitError> {
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "[{}][{}][{}] {}",
                chrono::Local::now().format("%Y-%m-%d %H:%M:%S"),
                record.level(),
                record.target(),
                message
            ))
        }).level(args.level_filter())
        .chain(std::io::stderr())
        .apply()?;

    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::from_args();
    init_logger(&args).unwrap();

    args.log();

    let files = args
        .files
        .iter()
        .filter_map(|path| path.to_str())
        .flat_map(|p_s| glob::glob(p_s).unwrap());

    for f in files {
        let f = f?;
        info!("Current file: {}", f.display());
        if !f.is_file() {
            warn!("{} is not a file", f.display());
            continue;
        }

        let mp3_info = Id3::new(f, &args.template);
        match mp3_info {
            Err(e) => error!("Could not read Id3, Reason: {}", e),
            Ok(mp3_info) => info!(
                "From template: {}",
                mp3_info.filename_from_template().display()
            ),
        }
    }

    Ok(())
}
