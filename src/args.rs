use std::path::PathBuf;

use log::LevelFilter;

/// id-three
/// Write from id3-tag to filename and vice versa
#[derive(StructOpt, Debug)]
#[structopt(name = "id-three")]
pub struct Args {
    /// Silent mode. Don't quit on error
    #[structopt(short = "s", long = "silent")]
    pub silent: bool,

    /// Verbose mode (-v, -vv, -vvv, etc.)
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    pub verbosity: u8,

    /// File template format (Either parse this template or rename files in this template). See -p for parameters
    #[structopt(short = "t", long = "template", default_value = "%n - %t")]
    pub template: String,

    /// Print available template parameters
    #[structopt(short = "p", long = "parameters")]
    pub parameters: bool,

    /// Parse id3-tag from filename
    #[structopt(short = "f", long = "from-filename")]
    pub from_filename: bool,

    /// Files to process
    #[structopt(name = "FILE", parse(from_os_str))]
    pub files: Vec<PathBuf>,
}

impl Args {
    pub fn level_filter(&self) -> LevelFilter {
        match self.verbosity {
            0 => LevelFilter::Warn,
            1 => LevelFilter::Info,
            2 => LevelFilter::Debug,
            _ => LevelFilter::Trace,
        }
    }

    pub fn log(&self) {
        let mut flags = String::new();
        if self.silent {
            flags.push('s');
        }
        for _ in 0..self.verbosity {
            flags.push('v');
        }
        if self.parameters {
            flags.push('p');
        }
        if self.from_filename {
            flags.push('f');
        }

        let options = format!(r#"-t "{}""#, self.template);

        if !flags.is_empty() {
            debug!("FLAGS: {}", flags);
        }

        debug!("OPTIONS: {}", options);
        for file in self.files.iter().map(|p| p.display()) {
            debug!("FILE: {}", file);
        }
    }
}
