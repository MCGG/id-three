use std::{path::PathBuf};

use id3::{self, Tag};

use crate::template::{TemplateArgs, TemplateParameter};

#[derive(Debug, Clone)]
pub struct Id3<'a> {
    pub filepath: PathBuf,
    tag: Tag,
    template: &'a str,
}

impl<'a> Id3<'a> {
    pub fn new(filepath: PathBuf, template: &'a str) -> id3::Result<Id3<'a>> {
        let tag = Tag::read_from_path(filepath.as_path())?;
        Ok(Id3 {
            filepath,
            tag,
            template,
        })
    }

    pub fn replace(&self, s: &str, t_param: TemplateParameter) -> String {
        use self::TemplateParameter::*;
        let t_str = t_param.as_str();
        match t_param {
            Title => s.replace(t_str, self.tag.title().unwrap_or_default()),
            TrackNumber => s.replace(
                t_str,
                &self.tag.track().map(|t| t.to_string()).unwrap_or_default(),
            ),
            Artist => s.replace(t_str, self.tag.artist().unwrap_or_default()),
            Album => s.replace(t_str, self.tag.album().unwrap_or_default()),
        }
    }

    pub fn filename_from_template(&self) -> PathBuf {
        let mut filename = String::from(self.template);

        for t_param in TemplateArgs::from(self.template) {
            filename = self.replace(&filename, t_param);
        }

        filename.into()
    }
}
